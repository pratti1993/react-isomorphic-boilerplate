import React from 'react';
import Base from './Example.base';
import Button from '../../atoms/Button';

export default class Example extends Base {
  constructor (props) {
    super(props);
  }

  render() {
    return (
      <div>
        <p>{this.state.count}</p>
        <Button onClick={this.handleDecrement}>Decrement</Button>
        <Button onClick={this.handleIncrement}>Increment</Button>
      </div>
    );
  }
}