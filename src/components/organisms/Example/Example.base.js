import React, {PureComponent} from 'react';

class Example extends PureComponent {
  constructor(props) {
    super(props);
    
    this.state = {
      count: 0,
    };
  }

  handleIncrement = () => {
    this.setState({
      count: this.state.count + 1,
    })
  }

  handleDecrement = () => {
    this.setState({
      count: this.state.count - 1,
    })
  }
}

export default Example;