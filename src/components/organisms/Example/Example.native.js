/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {Text, View} from 'react-native';
import Base from './Example.base';
import Button from '../../atoms/Button';

export default class extends Base {
  constructor (props) {
    super(props);
  }
  
  render() {
    return (
      <View>
        <Text>{this.state.count}</Text>
        <Button onPress={this.handleDecrement}>Decrement</Button>
        <Button onPress={this.handleIncrement}>Increment</Button>
      </View>
    );
  }
}