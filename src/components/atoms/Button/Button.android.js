import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

const Button = ({children, ...props}) => (
  <TouchableOpacity {...props}>
    <Text>{children}</Text>
  </TouchableOpacity>
);

export default Button;