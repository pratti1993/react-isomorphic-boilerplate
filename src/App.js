import React, {Component} from 'react';
import Example from './components/organisms/Example';

export default class App extends Component {
  render() {
    return (
      <div>
        <h4>Welcome React!</h4>
        <Example />
      </div>
    );
  }
}